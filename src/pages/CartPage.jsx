import React, { useState, useEffect } from 'react';
import PropTypes from "prop-types";
import { Button } from 'react-bootstrap';
import CardInfo from '../components/CardInfo';
import ModalWindow from '../components/ModalWindow';
import { useFunction } from '../hook/useFunction';

export default function CartPage() {
    const {deleteFromBasketUser} = useFunction();
    const [show1, setShow1] = useState(false);
    const [lastChosenProduct, setLastChosenProduct] = useState({});
    const [currentCard, setCurrentCard] = useState("");
    const {amountArray} = useFunction();
    const {basket} = useFunction();
    const {setBasket} = useFunction();

    const textFirstModal = "Are you sure you want to delete this product from the cart?";
    const headerFirstModal = "Are you sure?";
    return (
        <div className='cards'>
            {basket?.map((product) => (
                
                <div className="card" key={product.articul}>
                    
                    {<ModalWindow
                        header={headerFirstModal}
                        text={textFirstModal}
                        lastChosenProduct={lastChosenProduct}
                        closeButton={true}
                        color="red"
                        actions={
                            <>
                                <Button className='modal-button-left' onClick={ () => {
                                    
                                    deleteFromBasketUser(lastChosenProduct);
                                    setBasket(basket.filter(el => el.articul !== product.articul));
                                    setShow1(false);
                                }}>Ok
                                </Button>
                                 <Button className='modal-button-right' onClick={() => {setShow1(false)}}>
                                    No
                                </Button>
                            </>
                        }
                        handleShow={() => {setShow1(true)}}
                        handleClose={() => {setShow1(false)}}
                        show1={show1}
                        
                        />}
                        <CardInfo 
                            product={product}
                            button={
                                <>
                                <input type="number" id="quantity" name="quantity" min="1" max="30" defaultValue={product.amount}
                                onChange={(e) => {
                                    basket.forEach(el => {
                                        if(el.articul === product.articul) {
                                            el.amount = e.currentTarget.value;
                                            setBasket([...basket]);
                                        }
                                    })
                                }}/>
                                <svg className="cross-svg"
                                onClick={()=>{
                                    setShow1(true);
                                    setLastChosenProduct(product);
                                    
                                    
                                }}
                                width="50px" height="50px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16 8L8 16M8.00001 8L16 16" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                </svg>
                                </>
                            } 
                        />
                </div>
            ))}
            
        </div>
    );
}