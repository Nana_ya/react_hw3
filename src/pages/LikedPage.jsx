import React from 'react';
import PropTypes from "prop-types";
import ButtonModal from '../components/ButtonModal';
import CardInfo from '../components/CardInfo';
import { useFunction } from '../hook/useFunction';
import StarIcon from '../components/StarIcon';

export default function LikedPage(props) {
    const {unlikedProduct} = useFunction();
    return (
        <div className="cards">
                        {JSON.parse(localStorage.getItem('likedItems'))?.map((product) => (
                                <CardInfo 
                                    product={product} 
                                    class="card"
                                    key={product.articul}
                                    button={
                                        <StarIcon 
                                            fillLiked={"yellow"}
                                            product={product}
                                            remove="true"
                                        />}
                                />
                        ))}
                    </div>
    );
}