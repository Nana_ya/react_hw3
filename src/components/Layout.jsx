
import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import StarIcon from './StarIcon';
import CartIcon from './CartIcon';
import { useFunction } from '../hook/useFunction';

export default function Layout(props) {
    const {likedItems} = useFunction();
    const {basket} = useFunction();
    const {deleteFromBasketUser} = useFunction();
    const {unlikedProduct} = useFunction();
    return (
      <>
        <header>
          <Link to="/">Home</Link>
          <div className='flex-center'>
            <Link to="/liked"><StarIcon fill="yellow"/></Link>
            <span>{likedItems.length}</span>
            <div>
                <Link to="/cart"><CartIcon /></Link>
                <span>{basket.length}</span>
            </div>
        </div>
        </header>
        <Outlet />
      </>  
      
    );
}