import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from 'react-bootstrap/Button';
import ButtonModal from './ButtonModal';
import ModalWindow from './ModalWindow';
import CardInfo from "./CardInfo";

export default function Cards(props) {
    const [show1, setShow1] = useState(false);
    const [products, setProducts] = useState([]);
    const [lastChosenProduct, setLastChosenProduct] = useState({});

    useEffect(() => {
        fetch('./products.json')
         .then(response => response.json())
         .then(data => {
            setProducts(data.products);
            console.log("data", data.products);
         }
    )}, [])

    let firstModalButtons = 
        <>
            <Button className='modal-button-left' onClick={ () => {
                props.basket(lastChosenProduct);
                setShow1(false);
            }}>
                Ok
            </Button>
            <Button className='modal-button-right' onClick={() => {setShow1(false)}}>
                No
            </Button>
        </>


        const textFirstModal = "Are you sure you want to add this product to the cart?";
        const headerFirstModal = "Do you want to delete this file?";
          
        return (
            <div>
                <ModalWindow
                header={headerFirstModal}
                text={textFirstModal}
                closeButton={true}
                color="red"
                actions={firstModalButtons}
                handleShow={() => {setShow1(true)}}
                handleClose={() => {setShow1(false)}}
                show1={show1}
                lastChosenProduct={lastChosenProduct}
                />
            
                <div className="cards">
                    {products.map((product) => (
                        <div className="cards" key={product.articul}>
                            <CardInfo 
                            product={product}
                            likedItems={props.likedItems}
                            likedProduct={props.likedProduct}
                            unlikedProduct={props.unlikedProduct}
                            class="card"
                            button={
                                <ButtonModal
                                    backgroundColor={
                                        {
                                            backgroundColor: "red",
                                            border: 0,
                                        }
                                    }
                                    text={"Add to the cart"}
                                    onClick={()=>{
                                        setShow1(true);
                                        setLastChosenProduct(product);
                                    }}
                                ></ButtonModal >
                            } 
                        />
                
                        </div>
                    ))}
                </div>
    </div>
    );
}

Cards.propTypes = {
    basket: PropTypes.func,
    likedItems: PropTypes.array,
    likedProduct: PropTypes.func,
    unlikedProduct: PropTypes.func
}