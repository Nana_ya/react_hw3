import { createContext, useState, useEffect } from "react";

export const FunctionContext = createContext(null);

export const FunctionProvider = ({children}) => {
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || []);
    const [likedItems, setLikedItems] = useState(JSON.parse(localStorage.getItem('likedItems')) || []);
  
    useEffect(() => {
      localStorage.setItem("likedItems", JSON.stringify(likedItems));
    }, [likedItems])
  
    useEffect(() => {
      localStorage.setItem("basket", JSON.stringify(basket));
    }, [basket])
    
    function basketUser(item){
      let copy = false;
      basket.forEach(el => {
        if (el.articul === item.articul){
          copy = true;
        }
      })
      if(!copy){
        setBasket([...basket, item]);
        localStorage.setItem("basket", JSON.stringify(basket));
      }
    }
  
    function deleteFromBasketUser(item){
      let newBasketUserArray = JSON.parse(localStorage.getItem('basket')).filter(product => product.articul !== item.articul);
      setBasket(newBasketUserArray);
    }
    function likedProduct(item){
      setLikedItems([...likedItems, item]);
      localStorage.setItem("likedItems", JSON.stringify(likedItems));
    }
    function unlikedProduct(item){
      let newLikedArray = JSON.parse(localStorage.getItem('likedItems')).filter(product => product.articul !== item.articul);
      setLikedItems(newLikedArray);
    }

    const value = {basket, likedItems, setBasket, basketUser, deleteFromBasketUser, likedProduct, unlikedProduct}

    return (
      <FunctionContext.Provider value={value}>
        {children}
      </FunctionContext.Provider>
    )
}